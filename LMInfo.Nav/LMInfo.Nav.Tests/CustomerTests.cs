﻿using System;
using System.IO;
using NUnit.Framework;
using Newtonsoft.Json.Linq;

namespace LMInfo.Nav.Tests
{
    [TestFixture()]
    public class CustomerTests
    {
        [Test]
        public void TestGetReadCustomerSoapRequest()
        {
            string result = ProcessCustomer.GetReadCustomerSoapRequest("LMCU100216949");
            Console.WriteLine(result);
            Assert.NotNull(result);
        }

        [Test]
        public void TestGetCreateCustomerSoapRequest()
        {
            string jsonCustomer = string.Empty;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customer_create_test.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonCustomer = reader.ReadToEnd();
                reader.Close();
            }

            string result = ProcessCustomer.GetCreateCustomerSoapRequest(jsonCustomer);
            Console.WriteLine(result);
            Assert.NotNull(result);
        }

        [Test]
        public void TestGetUpdateCustomerSoapRequest()
        {
            string jsonCustomer = string.Empty;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "customer_update_test.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonCustomer = reader.ReadToEnd();
                reader.Close();
            }

            string result = ProcessCustomer.GetUpdateCustomerSoapRequest(jsonCustomer, "44;EgAAAAJ7/0wATQBDAFUAMQAwADAAMAA0ADYANAA4ADI=8;418335780;");
            Console.WriteLine(result);
            Assert.NotNull(result);
        }
    }
}
