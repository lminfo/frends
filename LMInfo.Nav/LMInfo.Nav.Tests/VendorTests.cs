﻿using System;
using System.IO;
using NUnit.Framework;
using Newtonsoft.Json.Linq;

namespace LMInfo.Nav.Tests
{
    [TestFixture()]
    public class VendorTests
    {
        [Test]
        public void TestGetReadVendorSoapRequest()
        {
            string result = ProcessVendor.GetReadVendorSoapRequest("LMOR1002466");
            Console.WriteLine(result);
            Assert.NotNull(result);
        }

        [Test]
        public void TestGetCreateVendorSoapRequest()
        {
            string jsonVendor = string.Empty;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "vendor_create_test.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonVendor = reader.ReadToEnd();
                reader.Close();
            }

            string result = ProcessVendor.GetCreateVendorSoapRequest(jsonVendor);
            Console.WriteLine(result);
            Assert.NotNull(result);
        }

        [Test]
        public void TestGetUpdateVendorSoapRequest()
        {
            string jsonVendor = string.Empty;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "vendor_update_test.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonVendor = reader.ReadToEnd();
                reader.Close();
            }

            string result = ProcessVendor.GetUpdateVendorSoapRequest(jsonVendor, "44;EgAAAAJ7/0wATQBDAFUAMQAwADAAMAA0ADYANAA4ADI=8;418335780;");
            Console.WriteLine(result);
            Assert.NotNull(result);
        }
    }
}
