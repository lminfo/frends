﻿using System;
using System.IO;
using NUnit.Framework;

namespace LMInfo.Nav.Tests
{
    [TestFixture]
    public class SalesInvoiceTests
    {
        [Test]
        public void TestImportSalesHeaderGenerator()
        {
            string jsonInvoice = string.Empty;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "salesinvoicetest.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonInvoice = reader.ReadToEnd();
                reader.Close();
            }
            string result = ProcessSalesInvoice.GetImportSalesHeaderSoapRequest(jsonInvoice);
            Console.WriteLine(result);
            Assert.NotNull(result);
        }

        [Test]
        public void TestImportSalesLineGenerator()
        {
            string jsonInvoice = string.Empty;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "salesinvoicetest.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonInvoice = reader.ReadToEnd();
                reader.Close();
            }
            string result = ProcessSalesInvoice.GetImportSalesLineSoapRequest(1, jsonInvoice);
            Console.WriteLine(result);
            Assert.NotNull(result);
        }
    }
}
