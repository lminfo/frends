﻿using Newtonsoft.Json.Linq;
using NUnit.Framework;
using System;
using System.IO;

namespace LMInfo.Nav.Tests
{
    [TestFixture()]
    public class PurchaseInvoiceTests
    {
        [Test]
        public void TestImportPurchaseHeaderGenerator()
        {
            string jsonInvoice = string.Empty;
            //string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "headertest.json");
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test_ocsaas_new2.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonInvoice = reader.ReadToEnd();
                reader.Close();
            }

            string result = ProcessPurchaseInvoice.GetImportPurchaseHeaderSoapRequest(jsonInvoice);
            Console.WriteLine(result);
            Assert.NotNull(result);
        }

        [Test]
        public void TestImportPurchaseLineGenerator()
        {
            string jsonFile = string.Empty;
            //string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "lineitemtest.json");
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "test_ocsaas_new2.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonFile = reader.ReadToEnd();
                reader.Close();
            }
            JObject json = JObject.Parse(jsonFile);
            string documentType = (string)json["documentType"];
            string invoiceNo = (string)json["documentNo"];
            string vendor = (string)json["vendor"];
            int lineNumber = (int)json["lineNumber"];
            JObject lineItem = (JObject)json["jsonLineItem"];

            string result = ProcessPurchaseInvoice.GetImportPurchaseLineSoapRequest(lineNumber, invoiceNo, documentType, vendor, lineItem.ToString());
            Console.WriteLine(result);
        }

        [Test]
        public void TestEOLImportPurchaseHeaderGenerator()
        {
            string jsonFile = string.Empty;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "eol_ledger.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonFile = reader.ReadToEnd();
                reader.Close();
            }

            JObject json = JObject.Parse(jsonFile);

            string postingNoSeries = "EOL";
            string documentType = "INVOICE";
            int voucher = (int)json["intTositeID"];
            string invoiceNumber = $"{postingNoSeries}1600000000{voucher}";
            string vendorNo = (string)json["strKustantajakoodi"];
            string vendorName = (string)json["strKustantajanNimi"];
            string postingDate = (string)json["dtLaskunpaiva"];
            string postingDescription = (string)json["strKustantajakoodi"];
            string dueDate = (string)json["dtErapaiva"];
            string currency = (string)json["strValuuttakoodi"];
            string vendorInvoiceNo = (string)json["strKustantajanlaskunumero"];
            string documentDate = (string)json["dtLaskunpaiva"];
            string invoiceAmount = (string)json["decSumma"];
            string invoiceMessage = (string)json["strKustantajanlaskunumero"];

            string result = ProcessPurchaseInvoice.GetLedgerImportPurchaseHeaderSoapRequest(
                documentType,
                invoiceNumber,
                vendorNo,
                vendorName,
                postingDate,
                postingDescription,
                dueDate,
                currency,
                vendorInvoiceNo,
                documentDate,
                invoiceAmount,
                invoiceMessage);

            Console.WriteLine(result);
            Assert.NotNull(result);
        }

        [Test]
        public void TestEOLImportPurchaseLineGenerator()
        {
            string jsonFile = string.Empty;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "eol_posting.json");

            using (StreamReader reader = File.OpenText(file))
            {
                jsonFile = reader.ReadToEnd();
                reader.Close();
            }

            JObject json = JObject.Parse(jsonFile);

            string postingNoSeries = "EOL";
            string documentType = "Invoice";
            int voucher = (int)json["intTositeId"];
            string documentNumber = $"{postingNoSeries}1600000000{voucher}";
            string vendorNo = "LMOR1001943";
            string accountNo = (string)json["strTili"];
            string shortcutDimension2Code = (string)json["strCountryPL"];
            int lineNumber = 1;
            string directUnitCost = (string)json["decSumma"];

            string result = ProcessPurchaseInvoice.GetLedgerImportPurchaseLineSoapRequest(
                documentType,
                documentNumber,
                vendorNo,
                accountNo,
                shortcutDimension2Code,
                lineNumber,
                directUnitCost);

            Console.WriteLine(result);
            Assert.NotNull(result);
        }
    }
}
