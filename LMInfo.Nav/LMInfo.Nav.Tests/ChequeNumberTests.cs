﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMInfo.Nav.Tests
{
    [TestFixture()]
    public class ChequeNumberTests
    {
        [Test]
        public void TestGetReadMultipleChequeNumberSoapRequest()
        {
            string result = ProcessChequeNumber.GetReadMultipleChequeNumberSoapRequest("UP31orders20200120");
            Console.WriteLine(result);
            Assert.NotNull(result);
        }

        [Test]
        public void TestGetUpdateChequeNumberSoapRequest()
        {
            string result = ProcessChequeNumber.GetUpdateChequeNumberSoapRequest("12;GQAAAACHBKI08;965189560;", "016322");
            Console.WriteLine(result);
            Assert.NotNull(result);
        }
    }
}
