﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LMInfo.Nav
{
    internal static class VendorSoapRequestGenerator
    {
        internal static string GenerateReadVendorSoapRequest(string no)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(string.Format(VendorSnippets.ImportVendorReadRequestSnippet, no));
            return doc.OuterXml;
        }

        internal static string GenerateVendorSoapRequest(
            bool update,
            string key,
            string no,
            string name,
            string name2,
            string address,
            string postCode,
            string city,
            string countryRegionCode,
            string eMail,
            string oldVendorNumber,
            string priority,
            string paymentMethodCode,
            string paymentTermsCode,
            string purchaserCode
            )
        {
            StringBuilder sb = new StringBuilder(2000);
            XmlDocument doc = new XmlDocument();

            if (update) { sb.Append(string.Format(VendorSnippets.Key, key)); }
            if (!string.IsNullOrEmpty(no)) { sb.Append(string.Format(VendorSnippets.No, no)); }
            if (!update && !string.IsNullOrEmpty(name)) { sb.Append(string.Format(VendorSnippets.Name, name.ToTitleCase())); }  
            if (!update && !string.IsNullOrEmpty(name2)) { sb.Append(string.Format(VendorSnippets.Name2, name2.ToTitleCase())); }
            if (!string.IsNullOrEmpty(address)) { sb.Append(string.Format(VendorSnippets.Address, address.ToTitleCase())); }
            if (!string.IsNullOrEmpty(postCode)) { sb.Append(string.Format(VendorSnippets.PostCode, postCode.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(city)) { sb.Append(string.Format(VendorSnippets.City, city.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(countryRegionCode)) { sb.Append(string.Format(VendorSnippets.CountryRegionCode, countryRegionCode)); }
            if (!string.IsNullOrEmpty(eMail)) { sb.Append(string.Format(VendorSnippets.EMail, eMail.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(oldVendorNumber)) { sb.Append(string.Format(VendorSnippets.OldVendorNumber, oldVendorNumber)); }
            if (!update && !string.IsNullOrEmpty(priority)) { sb.Append(string.Format(VendorSnippets.Priority, priority)); }
            if (!string.IsNullOrEmpty(paymentMethodCode)) { sb.Append(string.Format(VendorSnippets.PaymentMethodCode, paymentMethodCode)); }
            if (!string.IsNullOrEmpty(paymentTermsCode)) { sb.Append(string.Format(VendorSnippets.PaymentTermsCode, paymentTermsCode)); }
            if (!string.IsNullOrEmpty(purchaserCode)) { sb.Append(string.Format(VendorSnippets.PurchaserCode, purchaserCode)); }

            if (update)
            {
                doc.LoadXml(string.Format(VendorSnippets.ImportVendorUpdateRequestSnippet, sb));
            }
            else
            {
                doc.LoadXml(string.Format(VendorSnippets.ImportVendorCreateRequestSnippet, sb));
            }

            return doc.OuterXml;
        }

    }
}
