﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMInfo.Nav
{
    internal static class SalesInvoiceUtility
    {
        public static string GetDocumentType(string documentType)
        {
            string mappedType = String.Empty;

            if(!String.IsNullOrEmpty(documentType))
            {
                switch(documentType)
                {
                    case "Lasku":
                        mappedType = "Invoice";
                        break;
                    case "Hyvityslasku":
                        mappedType = "Credit_Memo";
                        break;
                }
            }

            return mappedType;
        }

        public static string GetInvoiceSentType(string invoiceSentType)
        {
            string mappedType = String.Empty;

            if (!String.IsNullOrEmpty(invoiceSentType))
            {
                switch (invoiceSentType)
                {
                    case "paperi":
                        mappedType = "Paperi";
                        break;
                    case "e-lasku":
                        mappedType = "e_lasku";
                        break;
                }
            }
            return mappedType;
        }

        public static string GetReferenceNo(string referenceNo)
        {
            int beginIndex = referenceNo.IndexOf('<') + 1;
            if (beginIndex != 0)
            {
                int endIndex = beginIndex;
                for(;endIndex != referenceNo.Length; endIndex++)
                {
                    if(!Char.IsDigit(referenceNo[endIndex]))
                    {
                        break;
                    }
                }
                return referenceNo.Substring(beginIndex, endIndex);
            }
            return referenceNo;
        }
    }
}
