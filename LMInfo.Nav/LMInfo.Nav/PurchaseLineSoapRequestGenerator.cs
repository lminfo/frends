﻿using System.Text;
using System.Xml;

namespace LMInfo.Nav
{
    public class PurchaseLineSoapRequestGenerator
    {

        public string DocumentType { get; set; }
        public string DocumentNumber { get; set; }
        public string BuyFromVendorNo { get; set; }
        public int LineNumber { get; set; }
        public string AccountNo { get; set; }
        public string DirectUnitCost { get; set; }
        public string ShortcutDimension1Code { get; set; }
        public string ShortcutDimension2Code { get; set; }
        public string ShortcutDimension3Code { get; set; }
        public string DeferralCode { get; set; }
        public string OcVatCode { get; set; }

        public string GeneratorImportPurchaseLineSoapRequest()
        {
            StringBuilder sb = new StringBuilder(1000);
            XmlDocument doc = new XmlDocument();


            DocumentType = PurchaseInvoiceUtility.GetDocumentType(DocumentType);
            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.DocumentType, DocumentType);

            DirectUnitCost = PurchaseInvoiceUtility.GetAmount(DocumentType, DirectUnitCost);
            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.DirectUnitCost, DirectUnitCost);

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.DocumentNo, DocumentNumber.MaxLength(20));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.BuyfromVendorNo, BuyFromVendorNo);

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.LineNo, LineNumber.ToString());

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.Type, "G_L_Account");

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.No, AccountNo.MaxLength(20));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.Quantity, "1");

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.ShortcutDimension1Code, ShortcutDimension1Code.MaxLength(20));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.ShortcutDimension2Code, ShortcutDimension2Code.MaxLength(20));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.ShortcutDimension3Code, ShortcutDimension3Code.MaxLength(20));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.DeferralCode, DeferralCode);

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.OCVatCode, OcVatCode);

            doc.LoadXml(string.Format(PurchaseInvoiceSnippets.ImportPurchaseLine, sb));
            return doc.OuterXml;
        }
    }
}
