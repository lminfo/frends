﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMInfo.Nav
{
    internal static class CustomerSnippets
    {
        internal static string ImportCustomerReadRequestSnippet =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_customerstonav"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Read>
                    <ws:No>
                      {0}
                    </ws:No>
                  </ws:Read>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string ImportCustomerCreateRequestSnippet =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_customerstonav"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Create>
                    <ws:WS_CustomersToNAV>
                      {0}
                    </ws:WS_CustomersToNAV>
                  </ws:Create>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string ImportCustomerUpdateRequestSnippet =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_customerstonav"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Update>
                    <ws:WS_CustomersToNAV>
                      {0}
                    </ws:WS_CustomersToNAV>
                  </ws:Update>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string Key = "<ws:Key>{0}</ws:Key>";
        internal static string No = "<ws:No>{0}</ws:No>";
        internal static string Name = "<ws:Name>{0}</ws:Name>";
        internal static string Name2 = "<ws:Name_2>{0}</ws:Name_2>";
        internal static string Address = "<ws:Address>{0}</ws:Address>";
        internal static string PostCode = "<ws:Post_Code>{0}</ws:Post_Code>";
        internal static string City = "<ws:City>{0}</ws:City>";
        internal static string CountryRegionCode = "<ws:Country_Region_Code>{0}</ws:Country_Region_Code>";
        internal static string PhoneNo = "<ws:Phone_No>{0}</ws:Phone_No>";
        internal static string EMail = "<ws:E_Mail>{0}</ws:E_Mail>";
        internal static string VATRegistrationNo = "<ws:VAT_Registration_No>{0}</ws:VAT_Registration_No>";
        internal static string ReminderTermsCode = "<ws:Reminder_Terms_Code>{0}</ws:Reminder_Terms_Code>";
        internal static string FinChargeTermsCode = "<ws:Fin_Charge_Terms_Code>{0}</ws:Fin_Charge_Terms_Code>";
        internal static string LanguageCode = "<ws:Language_Code>{0}</ws:Language_Code>";
        internal static string GlobalDimension2Code = "<ws:Global_Dimension_2_Code>{0}</ws:Global_Dimension_2_Code>";
        internal static string Sopimusasiakas = "<ws:Sopimusasiakas>{0}</ws:Sopimusasiakas>";
        internal static string OldCustomerNumber = "<ws:Old_Customer_Number>{0}</ws:Old_Customer_Number>";
        internal static string Priority = "<ws:Priority>{0}</ws:Priority>";
    }
}
