﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LMInfo.Nav
{
    internal static class CustomerSoapRequestGenerator
    {
        internal static string GenerateReadCustomerSoapRequest(string no)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(string.Format(CustomerSnippets.ImportCustomerReadRequestSnippet, no));
            return doc.OuterXml;
        }

        internal static string GenerateCustomerSoapRequest(
            bool update,
            string key,
            string no,
            string name,
            string name2,
            string address,
            string postCode,
            string city,
            string countryRegionCode,
            string phoneNo,
            string eMail,
            string vatRegistrationNo,
            string reminderTermsCode,
            string finChargeTermsCode,
            string languageCode,
            string globalDimension2Code,
            string sopimusasiakas,
            string oldCustomerNumber
            )
        {
            StringBuilder sb = new StringBuilder(2000);
            XmlDocument doc = new XmlDocument();

            if (update) { sb.Append(string.Format(CustomerSnippets.Key, key)); }
            if (!string.IsNullOrEmpty(no)) { sb.Append(string.Format(CustomerSnippets.No, no)); }
            if (!string.IsNullOrEmpty(name)) { sb.Append(string.Format(CustomerSnippets.Name, name.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(name2)) { sb.Append(string.Format(CustomerSnippets.Name2, name2.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(address)) { sb.Append(string.Format(CustomerSnippets.Address, address.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(postCode)) { sb.Append(string.Format(CustomerSnippets.PostCode, postCode.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(city)) { sb.Append(string.Format(CustomerSnippets.City, city.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(countryRegionCode)) { sb.Append(string.Format(CustomerSnippets.CountryRegionCode, countryRegionCode)); }
            if (!string.IsNullOrEmpty(phoneNo)) { sb.Append(string.Format(CustomerSnippets.PhoneNo, phoneNo.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(eMail)) { sb.Append(string.Format(CustomerSnippets.EMail, eMail.ReplaceCharsWithPredefinedEntities())); }
            if (!string.IsNullOrEmpty(vatRegistrationNo)) { sb.Append(string.Format(CustomerSnippets.VATRegistrationNo, vatRegistrationNo)); }
            if (!string.IsNullOrEmpty(reminderTermsCode)) { sb.Append(string.Format(CustomerSnippets.ReminderTermsCode, reminderTermsCode)); }
            if (!string.IsNullOrEmpty(finChargeTermsCode)) { sb.Append(string.Format(CustomerSnippets.FinChargeTermsCode, finChargeTermsCode)); }
            if (!string.IsNullOrEmpty(languageCode)) { sb.Append(string.Format(CustomerSnippets.LanguageCode, languageCode)); }
            if (!string.IsNullOrEmpty(globalDimension2Code)) { sb.Append(string.Format(CustomerSnippets.GlobalDimension2Code, globalDimension2Code)); }
            if (!string.IsNullOrEmpty(sopimusasiakas)) { sb.Append(string.Format(CustomerSnippets.Sopimusasiakas, sopimusasiakas)); }
            if (!string.IsNullOrEmpty(oldCustomerNumber)) { sb.Append(string.Format(CustomerSnippets.OldCustomerNumber, oldCustomerNumber)); }

            if (update)
            {
                doc.LoadXml(string.Format(CustomerSnippets.ImportCustomerUpdateRequestSnippet, sb));
            } else
            {
                doc.LoadXml(string.Format(CustomerSnippets.ImportCustomerCreateRequestSnippet, sb));
            }

            return doc.OuterXml;
        }
    }
}
