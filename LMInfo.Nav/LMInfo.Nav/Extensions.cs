﻿using System;
using System.Globalization;
using System.Text;
using Newtonsoft.Json.Linq;


namespace LMInfo.Nav
{
    internal static class Extensions
    {
        const string OutputDateFormat = "yyyy-MM-dd";

        internal static string FormatAsDateWithHypens(this string dateString, string inputDateFormat="yyyyMMdd")
        {
            if (String.IsNullOrEmpty(dateString))
            {
                return String.Empty;
            }

            return DateTime.ParseExact(dateString, inputDateFormat, CultureInfo.InvariantCulture).ToString(OutputDateFormat);
        }

        internal static string MaxLength(this string str, int maxLength)
        {
            if (!String.IsNullOrEmpty(str) && str.Length > maxLength)
            {
                return str.Substring(0, maxLength);
            }

            return str;
        }

        internal static string RemoveSpaces(this string str)
        {
            return str.Trim().Replace(" ", "");
        }

        internal static bool IsNullOrEmpty(this JToken token)
        {
            return (token == null) ||
                   (token.Type == JTokenType.Array && !token.HasValues) ||
                   (token.Type == JTokenType.Object && !token.HasValues) ||
                   (token.Type == JTokenType.String && token.ToString() == string.Empty) ||
                   (token.Type == JTokenType.Null);
        }

        internal static string ReplaceCharsWithPredefinedEntities(this string s)
        {
            s = s.Replace("&", "&amp;");
            s = s.Replace("<", "&lt;");
            s = s.Replace(">", "&gt;");
            s = s.Replace("\"", "&quot;");
            s = s.Replace("'", "&apos;");

            return s;
        }

        internal static StringBuilder AppendXmlSnippet(this StringBuilder sb, string snippet, string value)
        {
            if (!String.IsNullOrEmpty(value))
                sb.Append(string.Format(snippet, value.ReplaceCharsWithPredefinedEntities()));

            return sb;
        }

        internal static String ToTitleCase(this string s)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(s.ToLower()).ReplaceCharsWithPredefinedEntities();
        }
    }
}
