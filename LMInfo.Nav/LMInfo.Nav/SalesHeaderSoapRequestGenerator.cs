﻿using System;
using System.Text;
using System.Xml;

namespace LMInfo.Nav
{
    internal static class SalesHeaderSoapRequestGenerator
    {
        internal static string GenerateSalesHeaderSoapRequest(
            string documentType,
            string no,
            string sellToCustomerNo,
            string postingDate,
            string postingDescription,
            string dueDate,
            string customerPostingGroup,
            string currencyCode,
            string postingNo,
            string documentDate,
            string invoiceSentType,
            string invoiceAmount,
            string referenceNo,
            string netInvoiceAmount)
        {
            StringBuilder sb = new StringBuilder(2000);
            XmlDocument doc = new XmlDocument();

            sb.AppendXmlSnippet(SalesInvoiceSnippets.DocumentType, SalesInvoiceUtility.GetDocumentType(documentType));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.No, no.MaxLength(20));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.SellToCustomerNo, sellToCustomerNo.MaxLength(20));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.PostingDate, postingDate.FormatAsDateWithHypens("dd.MM.yyyy"));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.PostingDescription, postingDescription.MaxLength(50));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.DueDate, dueDate.FormatAsDateWithHypens("dd.MM.yyyy"));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.CustomerPostingGroup, customerPostingGroup.MaxLength(10));

            if(!String.IsNullOrEmpty(currencyCode) && currencyCode != "EUR")
            {
                sb.AppendXmlSnippet(SalesInvoiceSnippets.CurrencyCode, currencyCode.MaxLength(10));
            }

            sb.AppendXmlSnippet(SalesInvoiceSnippets.PostingNo, postingNo.MaxLength(20));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.DocumentDate, documentDate.FormatAsDateWithHypens("dd.MM.yyyy"));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.InvoiceSentType, SalesInvoiceUtility.GetInvoiceSentType(invoiceSentType));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.InvoiceAmount, invoiceAmount);
            sb.AppendXmlSnippet(SalesInvoiceSnippets.ReferenceNo,
                String.IsNullOrEmpty(referenceNo) ? "1" : SalesInvoiceUtility.GetReferenceNo(referenceNo.RemoveSpaces()).MaxLength(20));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.NetInvoiceAmount, netInvoiceAmount);

            doc.LoadXml(string.Format(SalesInvoiceSnippets.ImportSalesHeader, sb.ToString()));

            return doc.OuterXml;
        }
    }
}
