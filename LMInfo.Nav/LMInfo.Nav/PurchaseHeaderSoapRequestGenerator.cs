﻿using System;
using System.Text;
using System.Text.RegularExpressions;
using System.Xml;

namespace LMInfo.Nav
{
    public class PurchaseHeaderSoapRequestGenerator
    {
        public string DocumentType { get; set; }
        public string InvoiceAmount { get; set; }
        public string InvoiceNumber { get; set; }
        public string BuyFromVendorNo { get; set; }
        public string VendorName { get; set; }
        public string PostingDate { get; set; }
        public string PostingNo { get; set; }
        public string PostingDescription { get; set; }
        public string DueDate { get; set; }
        public string PaymentDiscountPercent { get; set; }
        public string PaymentDiscountDate { get; set; }
        public string CurrencyCode { get; set; }
        public string OnHold { get; set; }
        public string PostingNoSeries { get; set; }
        public string VendorInvoiceNo { get; set; }
        public string VenderCreditMemoNo { get; set; }
        public string DocumentDate { get; set; }
        public string PaymentRef { get; set; }
        public string InvoiceMessage { get; set; }
        public string InvoiceUrl { get; set; }
        public string Priority { get; set; }
        public string Comment { get; set; }
        public string OcIBAN { get; set; }
        public string OcBankAccountNo { get; set; }
        public string ConfirmedByUser { get; set; }
        public string TotalVatAmount { get; set; }
        public string DateFormat { get; set; } = "yyyyMMdd";

        public string GenerateImportPurchaseHeaderSoapRequest()
        {
            StringBuilder sb = new StringBuilder(2000);
            XmlDocument doc = new XmlDocument();

            DocumentType = PurchaseInvoiceUtility.GetDocumentType(DocumentType);
            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.DocumentType, DocumentType);

            InvoiceAmount = PurchaseInvoiceUtility.GetAmount(DocumentType, InvoiceAmount);
            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.InvoiceAmount, InvoiceAmount);

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.No, InvoiceNumber.MaxLength(20));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.BuyfromVendorNo, BuyFromVendorNo.MaxLength(20));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.VendorName, VendorName.MaxLength(50));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.PostingDate, PostingDate.FormatAsDateWithHypens(DateFormat));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.PostingNo, PostingNo.MaxLength(50));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.PostingDescription, PostingDescription.MaxLength(50));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.DueDate, DueDate.FormatAsDateWithHypens(DateFormat));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.PaymentDiscountPercent, PaymentDiscountPercent?.Replace(",", "."));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.PmtDiscountDate, PaymentDiscountDate.FormatAsDateWithHypens(DateFormat));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.CurrencyCode, CurrencyCode.MaxLength(10));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.OnHold, OnHold.MaxLength(10));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.PostingNoSeries, PostingNoSeries.MaxLength(35));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.VendorInvoiceNo, VendorInvoiceNo.MaxLength(35));

            if (DocumentType.Equals("Credit_Memo") && !string.IsNullOrEmpty(VenderCreditMemoNo))
            {
                sb.AppendXmlSnippet(PurchaseInvoiceSnippets.VendorCrMemoNo, VenderCreditMemoNo.MaxLength(35));
            }


            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.DocumentDate, DocumentDate.FormatAsDateWithHypens(DateFormat));

            if (!string.IsNullOrEmpty(PaymentRef))
            {
                sb.AppendXmlSnippet(PurchaseInvoiceSnippets.MessageType, "Reference_No");
                PaymentRef = PaymentRef.RemoveSpaces();
                if (!CurrencyCode.Equals("NOK"))
                {
                    PaymentRef = Regex.Replace(PaymentRef, "^0+(?!$)", "");
                }
                sb.AppendXmlSnippet(PurchaseInvoiceSnippets.InvoiceMessage, PaymentRef.MaxLength(30));
            }
            else if (!string.IsNullOrEmpty(InvoiceMessage))
            {
                sb.AppendXmlSnippet(PurchaseInvoiceSnippets.MessageType, "Message");
                sb.AppendXmlSnippet(PurchaseInvoiceSnippets.InvoiceMessage, InvoiceMessage.MaxLength(30));
            }
            else
            {
                sb.AppendXmlSnippet(PurchaseInvoiceSnippets.MessageType, "Message");
                if (!string.IsNullOrEmpty(VendorInvoiceNo))
                {
                    sb.AppendXmlSnippet(PurchaseInvoiceSnippets.InvoiceMessage, VendorInvoiceNo.MaxLength(30));
                }
            }

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.InvoiceURL, InvoiceUrl.MaxLength(250));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.Prioirity, Priority);

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.Comment, Comment.MaxLength(50));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.OCIBAN, OcIBAN.MaxLength(50));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.OCBankAccountNo, OcBankAccountNo.MaxLength(30));

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.ConfirmedbyUser, ConfirmedByUser);

            sb.AppendXmlSnippet(PurchaseInvoiceSnippets.TotalVatAmount, TotalVatAmount?.Replace("-", ""));

            doc.LoadXml(string.Format(PurchaseInvoiceSnippets.ImportPurchaseHeader, sb));
            return doc.OuterXml;
        }
    }
}
