﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMInfo.Nav
{
    internal static class ChequeNumberSnippets
    {
        internal static string ImportChequeNumberReadMultipleRequestSnippet =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_checkno"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:ReadMultiple>
                    <ws:filter>
                      <ws:Field>External_Document_No</ws:Field>
                      <ws:Criteria>{0}</ws:Criteria>
                    </ws:filter>
                  </ws:ReadMultiple>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string ImportChequeNumberUpdateRequestSnippet =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_checkno"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Update>
                    <ws:WS_CheckNo>
                      {0}
                    </ws:WS_CheckNo>
                  </ws:Update>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string Key = "<ws:Key>{0}</ws:Key>";
        internal static string CheckNo = "<ws:Check_No>{0}</ws:Check_No>";
    }
}
