﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace LMInfo.Nav
{
    public class ProcessSalesInvoice
    {
        /// <summary>
        ///     Takes JSON invoice from LibSystem and returns NAV Import Sales Invoice Header SOAP request.
        /// </summary>
        /// <param name="jsonInvoice">JSON Invoice</param>
        /// <returns>Returns NAV WS_ImportSalesHeader SOAP Request.</returns>
        public static string GetImportSalesHeaderSoapRequest(string jsonInvoice)
        {
            JObject invoice = JObject.Parse(jsonInvoice);
            string documentType = (string)invoice["header_Asiakirjatyyppi"];
            string no = invoice["header_Nro"].ToString();
            string sellToCustomerNo = (string)invoice["header_TilausasiakkaanNro"];
            string postingDate = (string)invoice["header_Kirjauspvm"];
            string postingDescription = (string)invoice["header_KirjauksenKuvaus"];
            string dueDate = (string)invoice["header_Erapaiva"];
            string customerPostingGroup = (string)invoice["header_AsiakkaanKirjausryhma"];
            string currencyCode = (string)invoice["header_ValuutanKoodi"];
            string postingNo = invoice["header_KirjauksenNro"].ToString();
            string documentDate = (string)invoice["header_AsiakirjanPvm"];
            string invoiceSentType = (string)invoice["header_LaskunLahetystapa"];
            string invoiceAmount = invoice["header_LaskunSumma"].ToString();
            string referenceNo = (string)invoice["header_Viitenro"];
            string netInvoiceAmount = invoice["header_LaskunSummaNetto"].ToString();

            return SalesHeaderSoapRequestGenerator.GenerateSalesHeaderSoapRequest(
                documentType,
                no,
                sellToCustomerNo,
                postingDate,
                postingDescription,
                dueDate,
                customerPostingGroup,
                currencyCode,
                postingNo,
                documentDate,
                invoiceSentType,
                invoiceAmount,
                referenceNo,
                netInvoiceAmount);
        }

        /// <summary>
        ///     Takes JSON invoice from LibSystem and returns NAV Import Sales Invoice Line SOAP request.
        /// </summary>
        /// <param name="lineNo">Line Number</param>
        /// <param name="jsonInvoice">JSON Invoice</param>
        /// <returns>Returns NAV WS_ImportSalesLine SOAP Request.</returns>
        public static string GetImportSalesLineSoapRequest(int lineNo, string jsonInvoice)
        {
            JObject invoice = JObject.Parse(jsonInvoice);
            string documentType = (string)invoice["row_Asiakirjatyyppi"];
            string documentNo = invoice["row_AsiakirjanNro"].ToString();
            string sellToCustomerNo = (string)invoice["row_TilausasiakkaanNro"];
            string type = "G_L_Account";
            string no = (string)invoice["row_Nro"];
            string quantity = invoice["row_Maara"].ToString();
            string unitPriceInclVat = invoice["row_YksikkohintaSisAlv"].ToString();

            return SalesLineSoapRequestGenerator.GenerateSalesLineSoapRequest(
                documentType,
                documentNo,
                sellToCustomerNo,
                lineNo.ToString(),
                type,
                no,
                quantity,
                unitPriceInclVat);
        }

    }
}
