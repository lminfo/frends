﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMInfo.Nav
{
    internal class PurchaseInvoiceUtility
    {
        public static string GetDocumentType(string documentType)
        {
            if (documentType.Equals("308") || documentType.Equals("381") || documentType.Equals("396") || documentType.Equals("502") || documentType.Equals("505"))
            {
                return "Credit_Memo";
            }
            return "Invoice";
        }

        public static string GetAmount(string documentType, string amount)
        {
            if (documentType.Equals("Credit_Memo"))
            {
                if (amount[0] == '-')
                {
                    amount = amount.Substring(1);
                }
                else
                {
                    amount = '-' + amount;
                }
            }
            return amount.Replace(',', '.');
        }
    }
}
