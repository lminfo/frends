﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMInfo.Nav
{
    public class ProcessCustomer
    {

        public static string GetReadCustomerSoapRequest(string no)
        {
            return CustomerSoapRequestGenerator.GenerateReadCustomerSoapRequest(no);
        }

        public static string GetCreateCustomerSoapRequest(string jsonCustomer)
        {
            JObject customer = JObject.Parse(jsonCustomer);

            string No = (string)customer["No"];
            string Name = (string)customer["Name"];
            string Name2 = (string)customer["Name2"];
            string Address = (string)customer["Address"];
            string PostCode = (string)customer["PostCode"];
            string City = (string)customer["City"];
            string CountryRegionCode = (string)customer["CountryRegionCode"];
            string PhoneNo = (string)customer["PhoneNo"];
            string Email = (string)customer["Email"];
            string VATRegistrationNo = (string)customer["VATRegistrationNo"];
            string ReminderTermsCode = (string)customer["ReminderTermsCode"];
            string FinChargeTermsCode = (string)customer["FinChargeTermsCode"];
            string LanguageCode = (string)customer["LanguageCode"];
            string GlobalDimension2Code = (string)customer["GlobalDimension2Code"];
            string Sopimusasiakas = (string)customer["Sopimusasiakas"].ToString();
            string OldCustomerNumber = (string)customer["OldCustomerNumber"].ToString();

            return CustomerSoapRequestGenerator.GenerateCustomerSoapRequest(
                false,
                "",
                No,
                Name,
                Name2,
                Address,
                PostCode,
                City,
                CountryRegionCode,
                PhoneNo,
                Email,
                VATRegistrationNo,
                ReminderTermsCode,
                FinChargeTermsCode,
                LanguageCode,
                GlobalDimension2Code,
                Sopimusasiakas,
                OldCustomerNumber
            );
        }

        public static string GetUpdateCustomerSoapRequest(string jsonCustomer, string key)
        {
            JObject customer = JObject.Parse(jsonCustomer);

            string Key = key;
            string No = (string)customer["No"];
            string Name = (string)customer["Name"];
            string Name2 = (string)customer["Name2"];
            string Address = (string)customer["Address"];
            string PostCode = (string)customer["PostCode"];
            string City = (string)customer["City"];
            string CountryRegionCode = (string)customer["CountryRegionCode"];
            string PhoneNo = (string)customer["PhoneNo"];
            string Email = (string)customer["Email"];
            string VATRegistrationNo = (string)customer["VATRegistrationNo"];
            string ReminderTermsCode = (string)customer["ReminderTermsCode"];
            string FinChargeTermsCode = (string)customer["FinChargeTermsCode"];
            string LanguageCode = (string)customer["LanguageCode"];
            string GlobalDimension2Code = (string)customer["GlobalDimension2Code"];
            string Sopimusasiakas = (string)customer["Sopimusasiakas"].ToString();
            string OldCustomerNumber = (string)customer["OldCustomerNumber"].ToString();

            return CustomerSoapRequestGenerator.GenerateCustomerSoapRequest(
                true,
                Key,
                No,
                Name,
                Name2,
                Address,
                PostCode,
                City,
                CountryRegionCode,
                PhoneNo,
                Email,
                VATRegistrationNo,
                ReminderTermsCode,
                FinChargeTermsCode,
                LanguageCode,
                GlobalDimension2Code,
                Sopimusasiakas,
                OldCustomerNumber
            );
        }

    }
}
