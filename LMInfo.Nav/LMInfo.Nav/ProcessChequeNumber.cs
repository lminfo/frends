﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMInfo.Nav
{
    public class ProcessChequeNumber
    {
        public static string GetReadMultipleChequeNumberSoapRequest(string externalDocumentNo)
        {
            return ChequeNumberSoapRequestGenerator.GenerateReadMultipleChequeNumberSoapRequest(externalDocumentNo);
        }

        public static string GetUpdateChequeNumberSoapRequest(string key, string checkNo)
        {
            return ChequeNumberSoapRequestGenerator.GenerateUpdateChequeNumberSoapRequest(key, checkNo);
        }
    }
}
