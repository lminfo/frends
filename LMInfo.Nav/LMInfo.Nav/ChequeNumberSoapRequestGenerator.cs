﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace LMInfo.Nav
{
    internal static class ChequeNumberSoapRequestGenerator
    {
        internal static string GenerateReadMultipleChequeNumberSoapRequest(string externalDocumentNo)
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(string.Format(ChequeNumberSnippets.ImportChequeNumberReadMultipleRequestSnippet, externalDocumentNo));
            return doc.OuterXml;
        }

        internal static string GenerateUpdateChequeNumberSoapRequest(string key, string checkNo)
        {
            StringBuilder sb = new StringBuilder(2000);
            XmlDocument doc = new XmlDocument();

            sb.Append(string.Format(CustomerSnippets.Key, key));
            if (!string.IsNullOrEmpty(checkNo)) { sb.Append(string.Format(ChequeNumberSnippets.CheckNo, checkNo)); }

            doc.LoadXml(string.Format(ChequeNumberSnippets.ImportChequeNumberUpdateRequestSnippet, sb));
            return doc.OuterXml;
        }


    }
}
