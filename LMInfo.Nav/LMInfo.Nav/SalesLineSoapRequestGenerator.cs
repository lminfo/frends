﻿using System;
using System.Text;
using System.Xml;

namespace LMInfo.Nav
{
    internal static class SalesLineSoapRequestGenerator
    {
        internal static string GenerateSalesLineSoapRequest(
            string documentType,
            string documentNo,
            string sellToCustomerNo,
            string lineNo,
            string type,
            string no,
            string quantity,
            string unitPriceInclVat)
        {
            StringBuilder sb = new StringBuilder(2000);
            XmlDocument doc = new XmlDocument();

            sb.AppendXmlSnippet(SalesInvoiceSnippets.DocumentType, SalesInvoiceUtility.GetDocumentType(documentType));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.DocumentNo, documentNo.MaxLength(20));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.SellToCustomerNo, sellToCustomerNo.MaxLength(20));
            if(!string.IsNullOrEmpty(documentNo))
            {
                sb.AppendXmlSnippet(SalesInvoiceSnippets.LineNo, lineNo);
            }
            sb.AppendXmlSnippet(SalesInvoiceSnippets.Type, type);
            sb.AppendXmlSnippet(SalesInvoiceSnippets.No, no.MaxLength(20));
            sb.AppendXmlSnippet(SalesInvoiceSnippets.Quantity, quantity);
            sb.AppendXmlSnippet(SalesInvoiceSnippets.UnitPriceInclVat, unitPriceInclVat);

            doc.LoadXml(string.Format(SalesInvoiceSnippets.ImportSalesLine, sb.ToString()));

            return doc.OuterXml;
        }
    }
}
