﻿namespace LMInfo.Nav
{
    internal static class PurchaseInvoiceSnippets
    {
        internal static string ImportPurchaseHeader =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_importpurchaseheader"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Create>
                    <ws:WS_ImportPurchaseHeader>
                      {0}
                    </ws:WS_ImportPurchaseHeader>
                  </ws:Create>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string ImportPurchaseLine =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_importpurchaseline"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Create>
                    <ws:WS_ImportPurchaseLine>
                      {0}
                    </ws:WS_ImportPurchaseLine>
                  </ws:Create>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string Key = "<ws:Key>{0}</ws:Key>";
        internal static string Status = "<ws:Status>{0}</ws:Status>";
        internal static string DocumentType = "<ws:Document_Type>{0}</ws:Document_Type>";
        internal static string No = "<ws:No>{0}</ws:No>";
        internal static string BuyfromVendorNo = "<ws:Buy_from_Vendor_No>{0}</ws:Buy_from_Vendor_No>";
        internal static string VendorName = "<ws:Vendor_Name>{0}</ws:Vendor_Name>";
        internal static string PostingDate = "<ws:Posting_Date>{0}</ws:Posting_Date>";
        internal static string PostingNo = "<ws:Posting_No>{0}</ws:Posting_No>";
        internal static string PostingDescription = "<ws:Posting_Description>{0}</ws:Posting_Description>";
        internal static string DueDate = "<ws:Due_Date>{0}</ws:Due_Date>";
        internal static string PaymentDiscountPercent = "<ws:Payment_Discount_Percent>{0}</ws:Payment_Discount_Percent>";
        internal static string PmtDiscountDate = "<ws:Pmt_Discount_Date>{0}</ws:Pmt_Discount_Date>";
        internal static string CurrencyCode = "<ws:Currency_Code>{0}</ws:Currency_Code>";
        internal static string OnHold = "<ws:On_Hold>{0}</ws:On_Hold>";
        internal static string PostingNoSeries = "<ws:Posting_No_Series>{0}</ws:Posting_No_Series>";
        internal static string VendorInvoiceNo = "<ws:Vendor_Invoice_No>{0}</ws:Vendor_Invoice_No>";
        internal static string VendorCrMemoNo = "<ws:Vendor_Cr_Memo_No>{0}</ws:Vendor_Cr_Memo_No>";
        internal static string DocumentDate = "<ws:Document_Date>{0}</ws:Document_Date>";
        internal static string InvoiceAmount = "<ws:Invoice_Amount>{0}</ws:Invoice_Amount>";
        internal static string MessageType = "<ws:Message_Type>{0}</ws:Message_Type>";
        internal static string InvoiceMessage = "<ws:Invoice_Message>{0}</ws:Invoice_Message>";
        internal static string Errortext = "<ws:Error_text>{0}</ws:Error_text>";
        internal static string LineErrorText = "<ws:Line_Error_Text>{0}</ws:Line_Error_Text>";
        internal static string InvoiceURL = "<ws:Invoice_URL>{0}</ws:Invoice_URL>";
        internal static string Prioirity = "<ws:Prioirity>{0}</ws:Prioirity>";
        internal static string OldVendorNumber = "<ws:Old_Vendor_Number>{0}</ws:Old_Vendor_Number>";
        internal static string Comment = "<ws:Comment>{0}</ws:Comment>";
        internal static string OCIBAN = "<ws:OC_IBAN>{0}</ws:OC_IBAN>";
        internal static string OCBankAccountNo = "<ws:OC_Bank_Account_No>{0}</ws:OC_Bank_Account_No>";
        internal static string ConfirmedbyUser = "<ws:Confirmed_by_User>{0}</ws:Confirmed_by_User>";
        internal static string CheckNo = "<ws:Check_No>{0}</ws:Check_No>";
        internal static string TotalVatAmount = "<ws:Total_VAT_Amount>{0}</ws:Total_VAT_Amount>";
        internal static string DocumentNo = "<ws:Document_No>{0}</ws:Document_No>";
        internal static string LineNo = "<ws:Line_No>{0}</ws:Line_No>";
        internal static string Type = "<ws:Type>{0}</ws:Type>";
        internal static string Quantity = "<ws:Quantity>{0}</ws:Quantity>";
        internal static string DirectUnitCost = "<ws:Direct_Unit_Cost>{0}</ws:Direct_Unit_Cost>";
        internal static string ShortcutDimension1Code = "<ws:Shortcut_Dimension_1_Code>{0}</ws:Shortcut_Dimension_1_Code>";
        internal static string ShortcutDimension2Code = "<ws:Shortcut_Dimension_2_Code>{0}</ws:Shortcut_Dimension_2_Code>";
        internal static string DeferralCode = "<ws:Deferral_Code>{0}</ws:Deferral_Code>";
        internal static string ShortcutDimension3Code = "<ws:Shortcut_Dimension_3_Code>{0}</ws:Shortcut_Dimension_3_Code>";
        internal static string ShortcutDimension4Code = "<ws:Shortcut_Dimension_4_Code>{0}</ws:Shortcut_Dimension_4_Code>";
        internal static string OCVatCode = "<ws:OC_VAT_Code>{0}</ws:OC_VAT_Code>";
    }
}
