﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;

namespace LMInfo.Nav
{
    /// <summary>
    ///     NAV Purchase Invoice Tasks
    /// </summary>
    public class ProcessPurchaseInvoice
    {
        /// <summary>
        ///     Takes JSON invoice from Opus Capita and returns NAV Import Purchase Invoice Header SOAP request.
        /// </summary>
        /// <param name="jsonInvoice">JSON Invoice</param>
        /// <returns>Returns NAV WS_ImportPurchaseHeader SOAP Request.</returns>
        public static string GetImportPurchaseHeaderSoapRequest(string jsonInvoice)
        {
            JObject invoice = JObject.Parse(jsonInvoice);
            JObject biller = invoice["BILLER"].ToObject<JObject>();
            string documentType = (string)invoice["INV"]["INVTYPE"];
            string invoiceNo = (string)invoice["INV"]["ELECTRA_ID"];
            string postingNo = string.Empty;
            string postingDate = (string)invoice["INV"]["ENTRY_DATE"];
            string customerName = (string)invoice["BILLER"]["CUSTOMER_INFORMATION"]["CUSTOMER_NAME"];
            string invId = (string)invoice["INV"]["INVID"];
            string postingDescription = $"{customerName} {invId}";
            string dueDate = (string)invoice["INV"]["DUE_DATE"];
            string paymentDiscountPercent = string.Empty;
            string paymentDiscountDate = string.Empty;
            if (!invoice["IND"]["CASH_DISCOUNT"].IsNullOrEmpty())
            {
                paymentDiscountPercent = (string)invoice["IND"]["CASH_DISCOUNT"]["PER_CENT"];
                paymentDiscountDate = (string)invoice["IND"]["CASH_DISCOUNT"]["DATE"];
            }
            string currencyCode = (string)invoice["INV"]["CURRENCY"]["CODE"];
            if (currencyCode == "EUR")
            {
                currencyCode = String.Empty;
            }
            string postingNoSeries = "IPA";
            if (!invoice["INV"]["RECTYPE"].IsNullOrEmpty())
            {
                postingNoSeries = "OC";
            } 
            string vendorInvoiceNo = invId;
            string vendorCreditMemoNo = invId;
            string documentDate = (string)invoice["IND"]["INVOICE_DATE"];
            string invoiceAmount = (string)invoice["INV"]["GROSS_AMOUNT"]["#text"];
            string countryCode = (string)invoice["BILLER"]["CUSTOMER_INFORMATION"]["COUNTRY_CODE"];
            string onHold = string.Empty;
            string totalVat = string.Empty;
            string paymentRef;
            int orderReference;
            if (!invoice["IND"]["ORDER_REFERENCE"].IsNullOrEmpty())
            {
                int.TryParse(invoice["IND"]["ORDER_REFERENCE"].ToString(), out orderReference);
            } else
            {
                orderReference = 3;
            }

            switch (countryCode)
            {
                case "FI":
                case "SE":
                case "NO":
                case "DK":
                    if (biller.ToString(Newtonsoft.Json.Formatting.None).Contains("DETAILS_OF_PAYMENT")) {
                        JObject detailsOfInvoice = biller["DETAILS_OF_PAYMENT"].ToObject<JObject>();
                        paymentRef = detailsOfInvoice.ToString(Newtonsoft.Json.Formatting.None).Contains("#text") ? (string)detailsOfInvoice["FI_PAYMENT_REFERENCE"]["#text"] : string.Empty;
                        /*
                        if (detailsOfInvoice.ToString(Newtonsoft.Json.Formatting.None).Contains("#text"))
                        {
                            paymentRef = (string)detailsOfInvoice["FI_PAYMENT_REFERENCE"]["#text"];
                        } else
                        {
                            paymentRef = (string)detailsOfInvoice["FI_PAYMENT_REFERENCE"];
                        }
                        */
                    }
                    else
                    {
                        paymentRef = string.Empty;
                    }
                    break;
                default:
                    paymentRef = string.Empty;
                    break;
            }
            string invoiceMessage;
            try
            {
                invoiceMessage = (string)invoice["INV"]["MESSAGE"];
            }
            catch
            {

                invoiceMessage = String.Empty;
            }

            string year = DateTime.Now.ToString("yyyy");
            string month = DateTime.Now.ToString("MM");
            string day = DateTime.Now.ToString("dd");
            string invoiceUrl = $"https://s3-eu-west-1.amazonaws.com/lminfo-data/opus_capita/invoice_pdf/{year}/{month}/{day}/imagesToLM_{invoiceNo}.pdf";

            int priority;
            try
            {
                priority = (int)invoice["IND"]["CONTRACT_INFORMATION"]["CONTRACT_NUMBER"];
                priority = SetPriority(priority);
            }
            catch
            {
                priority = CreatePriority(customerName, orderReference);
            }
            string comment;
            try
            {
                comment = (string)invoice["IND"]["CONTRACT_INFORMATION"]["CONTRACT_NUMBER"]["#text"];
            }
            catch
            {

                comment = String.Empty;
            }
            string identificationScheme;
            try
            {
                identificationScheme = (string)invoice["BILLER"]["BANKS"]["BANK_ACCOUNT_NUMBER"]["@IdentificationSchemeName"];
            }
            catch
            {
                identificationScheme = String.Empty;
            }
            string bankAccountNumber;
            try
            {
                bankAccountNumber = (string)invoice["BILLER"]["BANKS"]["BANK_ACCOUNT_NUMBER"]["#text"];
            }
            catch
            {
                bankAccountNumber = String.Empty;
            }
            string ocIBAN = string.Empty;
            string ocBankAccountNumber = string.Empty;
            if (identificationScheme.Equals("IBAN"))
            {
                ocIBAN = bankAccountNumber;
            }
            else
            {
                ocBankAccountNumber = bankAccountNumber;
            }

            //JArray operations = (JArray)invoice["HISTORY"]["OPERATION"];
            //string confirmedByUser = IsChange(operations) ? "true" : string.Empty;
            string confirmedByUser = string.Empty;

            string vendor = (string)invoice["BILLER"]["CUSTOMER_INFORMATION"]["CUSTOMER_CODE"];
            string buyFromVendorNumber = GetBuyFromVendorNo(vendor);

            var soapRequestGenerator = new PurchaseHeaderSoapRequestGenerator
            {
                DocumentType = documentType,
                InvoiceAmount = invoiceAmount,
                InvoiceNumber = invoiceNo,
                BuyFromVendorNo = buyFromVendorNumber,
                VendorName = vendor,
                PostingDate = postingDate,
                PostingNo = postingNo,
                PostingDescription = postingDescription,
                DueDate = dueDate,
                PaymentDiscountPercent = paymentDiscountPercent,
                PaymentDiscountDate = paymentDiscountDate,
                CurrencyCode = currencyCode,
                OnHold = onHold,
                PostingNoSeries = postingNoSeries,
                VendorInvoiceNo = vendorInvoiceNo,
                VenderCreditMemoNo = vendorCreditMemoNo,
                DocumentDate  =documentDate,
                PaymentRef = paymentRef,
                InvoiceMessage = invoiceMessage,
                InvoiceUrl = invoiceUrl,
                Priority = priority.ToString(),
                Comment = comment,
                OcIBAN = ocIBAN,
                OcBankAccountNo = ocBankAccountNumber,
                ConfirmedByUser = confirmedByUser,
                TotalVatAmount = totalVat
            };

            string soapRequest = soapRequestGenerator.GenerateImportPurchaseHeaderSoapRequest();

            return soapRequest;
        }

        /// <summary>
        ///     Takes JSON invoice line item from Opus Capita and returns NAV Import Purchase Invoice Line SOAP request.
        /// </summary>
        /// <param name="lineNumber">Line Number</param>
        /// <param name="documentNumber">Document Number</param>
        /// <param name="documentType">Document Type</param>
        /// <param name="vendor">Vendor</param>
        /// <param name="jsonLineItem">JSON Invoice</param>
        /// <returns>Returns NAV WS_ImportPurchaseLine SOAP Request.</returns>
        public static string GetImportPurchaseLineSoapRequest(int lineNumber, string documentNumber, string documentType, string vendor, string jsonLineItem)
        {
            JObject lineItem = JObject.Parse(jsonLineItem);
            string buyFromVendorNumber = GetBuyFromVendorNo(vendor);
            string directUnitCost = (string)lineItem["GROSS_AMOUNT"];
            JArray dimensions = (JArray)lineItem["DIMENSIONS"]["DIMENSION"];
            string accountNo = GetDimensionValue(dimensions, "ACC");
            string shortcutDimension1Code = GetDimensionValue(dimensions, "DEPT");
            string shortcutDimension2Code = GetDimensionValue(dimensions, "DIM1");
            string shortcutDimension3Code = GetDimensionValue(dimensions, "GROUP");
            string deferralCode = GetDimensionValue(dimensions, "PERIC");
            string ocVatCode = GetDimensionValue(dimensions, "VAT");

            var soapRequestGenerator = new PurchaseLineSoapRequestGenerator
            {
                DocumentType = documentType,
                DocumentNumber = documentNumber,
                BuyFromVendorNo = buyFromVendorNumber,
                LineNumber = lineNumber,
                AccountNo = accountNo,
                DirectUnitCost = directUnitCost,
                ShortcutDimension1Code = shortcutDimension1Code,
                ShortcutDimension2Code = shortcutDimension2Code,
                ShortcutDimension3Code = shortcutDimension3Code,
                DeferralCode = deferralCode,
                OcVatCode = ocVatCode
            };

            string soapRequest = soapRequestGenerator.GeneratorImportPurchaseLineSoapRequest();
            return soapRequest;
        }

        /// <summary>
        ///     Takes EOL ledger data and returns NAV Import Purchase Invoice Header SOAP request.
        /// </summary>
        /// <param name="documentType">Document Type</param>
        /// <param name="invoiceNumber">Invoice Number</param>
        /// <param name="vendorNo">Buy From Vendor No</param>
        /// <param name="vendorName">Vendor Name</param>
        /// <param name="postingDate">Posting Date</param>
        /// <param name="postingDescription">Posting Description</param>
        /// <param name="dueDate">Due Date</param>
        /// <param name="currency">Currency</param>
        /// <param name="vendorInvoiceNo">Vendor Invoice No</param>
        /// <param name="documentDate">Document Date</param>
        /// <param name="invoiceAmount">Invoice Amount</param>
        /// <param name="invoiceMessage">Invoice Message</param>
        /// <returns>Returns NAV WS_ImportPurchaseHeader SOAP Request.</returns>
        public static string GetLedgerImportPurchaseHeaderSoapRequest(
            string documentType,
            string invoiceNumber,
            string vendorNo,
            string vendorName,
            string postingDate,
            string postingDescription,
            string dueDate,
            string currency,
            string vendorInvoiceNo,
            string documentDate,
            string invoiceAmount,
            string invoiceMessage)
        {
            var soapRequestGenerator = new PurchaseHeaderSoapRequestGenerator
            {
                DocumentType = documentType,
                InvoiceNumber = invoiceNumber,
                BuyFromVendorNo = vendorNo,
                VendorName = vendorName,
                PostingDate = postingDate,
                PostingDescription = postingDescription,
                DueDate = dueDate,
                CurrencyCode = currency == "EUR" ? String.Empty : currency,
                VendorInvoiceNo = vendorInvoiceNo,
                DocumentDate = documentDate,
                InvoiceAmount = invoiceAmount?.Replace(',', '.'),
                InvoiceMessage = invoiceMessage,
                DateFormat = "dd.MM.yyyy"
            };

            return soapRequestGenerator.GenerateImportPurchaseHeaderSoapRequest();
        }

        public static string GetLedgerImportPurchaseLineSoapRequest(
            string documentType,
            string documentNumber,
            string vendorNo,
            string accountNo,
            string shortcutDimension2Code,
            int lineNumber,
            string directUnitCost)
        {
            var soapRequestGenerator = new PurchaseLineSoapRequestGenerator
            {
                DocumentType = documentType,
                DocumentNumber = documentNumber,
                BuyFromVendorNo = vendorNo,
                LineNumber = lineNumber,
                AccountNo = accountNo,
                DirectUnitCost = directUnitCost?.Replace(',', '.'),
                ShortcutDimension2Code = shortcutDimension2Code
            };

            return soapRequestGenerator.GeneratorImportPurchaseLineSoapRequest();
        }

        static int CreatePriority(string customerName, int orderReference)
        {
            int pos = customerName.IndexOf("|", StringComparison.InvariantCulture);
            string priority = customerName.Substring(pos + 1);
            int result;
            if (int.TryParse(priority, out result))
            {
                if (orderReference != 0 && orderReference < result)
                    return orderReference;
                
                return SetPriority(result);
            }

            return SetPriority(orderReference);
        }

        static int SetPriority(int priority)
        {
            if (priority < 1 || priority > 5)
            {
                return 3;
            }

            return priority;
        }

        static bool IsChange(JArray operations)
        {
            List<string> statusList = operations.Where(o => (int)o["HISTORYFLAG"] == 13).Select(o => (string)o["STATUS_TEXT"]).ToList();
            foreach (string status in statusList)
            {
                if (status.Equals("CHANGE"))
                {
                    return true;
                }
            }
            return false;
        }

        static string GetBuyFromVendorNo(string vendor)
        {
            if (!string.IsNullOrEmpty(vendor) && vendor.Length > 3)
            {
                string payMethod = vendor.Substring(vendor.Length - 4);
                switch (payMethod)
                {
                    case " TRA":
                    case " ICE":
                    case " PRE":
                    case " PRO":
                        return vendor.Substring(0, vendor.Length - 4);
                }
            }
            return vendor;
        }

        static string GetDimensionValue(JArray dimensions, string dimensionType)
        {
            return dimensions.Where(d => ((string)d["DIMENSION_TYPE"]).Equals(dimensionType)).Select(d => (string)d["DIMENSION_VALUE"]).FirstOrDefault();
        }
    }
}
