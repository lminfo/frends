﻿namespace LMInfo.Nav
{
    internal static class SalesInvoiceSnippets
    {
        internal static string ImportSalesHeader =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_importsalesheader"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Create>
                    <ws:WS_ImportSalesHeader>
                      {0}
                    </ws:WS_ImportSalesHeader>
                  </ws:Create>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string ImportSalesLine =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_importsalesline"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Create>
                    <ws:WS_ImportSalesLine>
                      {0}
                    </ws:WS_ImportSalesLine>
                  </ws:Create>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string Key = "<ws:Key>{0}</ws:Key>";
        internal static string DocumentType = "<ws:Document_Type>{0}</ws:Document_Type>";
        internal static string No = "<ws:No>{0}</ws:No>";
        internal static string SellToCustomerNo = "<ws:Sell_to_Customer_No>{0}</ws:Sell_to_Customer_No>";
        internal static string PostingDate = "<ws:Posting_Date>{0}</ws:Posting_Date>";
        internal static string PostingDescription = "<ws:Posting_Description>{0}</ws:Posting_Description>";
        internal static string DueDate = "<ws:Due_Date>{0}</ws:Due_Date>";
        internal static string CustomerPostingGroup = "<ws:Customer_Posting_Group>{0}</ws:Customer_Posting_Group>";
        internal static string CurrencyCode = "<ws:Currency_Code>{0}</ws:Currency_Code>";
        internal static string PostingNo = "<ws:Posting_No>{0}</ws:Posting_No>";
        internal static string DocumentDate = "<ws:Document_Date>{0}</ws:Document_Date>";
        internal static string InvoiceSentType = "<ws:Invoice_Sent_Type>{0}</ws:Invoice_Sent_Type>";
        internal static string InvoiceAmount = "<ws:Invoice_Amount>{0}</ws:Invoice_Amount>";
        internal static string ReferenceNo = "<ws:Reference_No>{0}</ws:Reference_No>";
        internal static string NetInvoiceAmount = "<ws:Net_Invoice_Amount>{0}</ws:Net_Invoice_Amount>";
        internal static string DocumentNo = "<ws:Document_No>{0}</ws:Document_No>";
        internal static string LineNo = "<ws:Line_No>{0}</ws:Line_No>";
        internal static string Type = "<ws:Type>{0}</ws:Type>";
        internal static string Quantity = "<ws:Quantity>{0}</ws:Quantity>";
        internal static string UnitPriceInclVat = "<ws:Unit_Price_Incl_VAT>{0}</ws:Unit_Price_Incl_VAT>";


    }
}
