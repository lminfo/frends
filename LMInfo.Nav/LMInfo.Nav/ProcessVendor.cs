﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMInfo.Nav
{
    public class ProcessVendor
    {

        public static string GetReadVendorSoapRequest(string no)
        {
            return VendorSoapRequestGenerator.GenerateReadVendorSoapRequest(no);
        }

        public static string GetCreateVendorSoapRequest(string jsonVendor)
        {
            JObject vendor = JObject.Parse(jsonVendor);

            string No = (string)vendor["No"];
            string Name = (string)vendor["Name"];
            string Name2 = (string)vendor["Name2"];
            string Address = (string)vendor["Address"];
            string PostCode = (string)vendor["PostCode"];
            string City = (string)vendor["City"];
            string CountryRegionCode = (string)vendor["CountryRegionCode"];
            string Email = (string)vendor["Email"];
            string OldVendorNumber = (string)vendor["OldVendorNumber"];
            string Priority = (string)vendor["Priority"].ToString();
            string PaymentMethodCode = (string)vendor["PaymentMethodCode"];
            string PaymentTermsCode = (string)vendor["PaymentTermsCode"];
            string PurchaserCode = (string)vendor["PurchaserCode"];

            return VendorSoapRequestGenerator.GenerateVendorSoapRequest(
                false,
                "",
                No,
                Name,
                Name2,
                Address,
                PostCode,
                City,
                CountryRegionCode,
                Email,
                OldVendorNumber,
                Priority,
                PaymentMethodCode,
                PaymentTermsCode,
                PurchaserCode
                );
        }

        public static string GetUpdateVendorSoapRequest(string jsonVendor, string key)
        {
            JObject vendor = JObject.Parse(jsonVendor);

            string Key = key;
            string No = (string)vendor["No"];
            string Name = (string)vendor["Name"];
            string Name2 = (string)vendor["Name2"];
            string Address = (string)vendor["Address"];
            string PostCode = (string)vendor["PostCode"];
            string City = (string)vendor["City"];
            string CountryRegionCode = (string)vendor["CountryRegionCode"];
            string Email = (string)vendor["Email"];
            string OldVendorNumber = (string)vendor["OldVendorNumber"];
            string Priority = (string)vendor["Priority"].ToString();
            string PaymentMethodCode = (string)vendor["PaymentMethodCode"];
            string PaymentTermsCode = (string)vendor["PaymentTermsCode"];
            string PurchaserCode = (string)vendor["PurchaserCode"];

            return VendorSoapRequestGenerator.GenerateVendorSoapRequest(
                true,
                Key,
                No,
                Name,
                Name2,
                Address,
                PostCode,
                City,
                CountryRegionCode,
                Email,
                OldVendorNumber,
                Priority,
                PaymentMethodCode,
                PaymentTermsCode,
                PurchaserCode
                );
        }
    }
}
