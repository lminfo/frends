﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LMInfo.Nav
{
    internal static class VendorSnippets
    {
        internal static string ImportVendorReadRequestSnippet =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_vendorstonav"">
                <soapenv:Header/>
                <soapenv:Body>
                    <ws:Read>
                    <ws:No>
                        {0}
                    </ws:No>
                    </ws:Read>
                </soapenv:Body>
            </soapenv:Envelope>";

        internal static string ImportVendorCreateRequestSnippet =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_vendorstonav"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Create>
                    <ws:WS_VendorsToNAV>
                      {0}
                    </ws:WS_VendorsToNAV>
                  </ws:Create>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string ImportVendorUpdateRequestSnippet =
            @"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
                xmlns:ws=""urn:microsoft-dynamics-schemas/page/ws_vendorstonav"">
                <soapenv:Header/>
                <soapenv:Body>
                  <ws:Update>
                    <ws:WS_VendorsToNAV>
                      {0}
                    </ws:WS_VendorsToNAV>
                  </ws:Update>
                </soapenv:Body>
              </soapenv:Envelope>";

        internal static string Key = "<ws:Key>{0}</ws:Key>";
        internal static string No = "<ws:No>{0}</ws:No>";
        internal static string Name = "<ws:Name>{0}</ws:Name>";
        internal static string Name2 = "<ws:Name_2>{0}</ws:Name_2>";
        internal static string Address = "<ws:Address>{0}</ws:Address>";
        internal static string PostCode = "<ws:Post_Code>{0}</ws:Post_Code>";
        internal static string City = "<ws:City>{0}</ws:City>";
        internal static string CountryRegionCode = "<ws:Country_Region_Code>{0}</ws:Country_Region_Code>";
        internal static string EMail = "<ws:E_Mail>{0}</ws:E_Mail>";
        internal static string OldVendorNumber = "<ws:Old_Vendor_Number>{0}</ws:Old_Vendor_Number>";
        internal static string Priority = "<ws:Priority>{0}</ws:Priority>";
        internal static string PaymentMethodCode = "<ws:Payment_Method_Code>{0}</ws:Payment_Method_Code>";
        internal static string PaymentTermsCode = "<ws:Payment_Terms_Code>{0}</ws:Payment_Terms_Code>";
        internal static string PurchaserCode = "<ws:Purchaser_Code>{0}</ws:Purchaser_Code>";

    }
}
