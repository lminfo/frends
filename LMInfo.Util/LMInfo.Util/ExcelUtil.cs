﻿using Newtonsoft.Json;
using OfficeOpenXml;
using System;
using System.Data;
using System.IO;

namespace LMInfo.Util
{
    /// <summary>
    ///     Convert JSON string to Excel
    /// </summary>
    public class ExcelUtil
    {

        /// <summary>
        ///     Takes JSON string and converts it to DataTable object
        /// </summary>
        /// <param name="json">JSON string</param>
        /// <returns>Returns DataTable</returns>
        private static DataTable ConvertJsonToDataTable(string json)
        {
            DataTable table = (DataTable)JsonConvert.DeserializeObject(json, (typeof(DataTable)));
            return table;
        }

        /// <summary>
        ///     Takes JSON string, converts it to Excel and save the file in path provided
        /// </summary>
        /// <param name="json">JSON string</param>
        /// <param name="path">Path string</param>
        /// <returns>Returns true if success, false if failure</returns>
        public static Boolean WriteToExcel(string json, string path)
        {
            try
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                DataTable table = ConvertJsonToDataTable(json);

                FileInfo f = new FileInfo(path);
                ExcelPackage package = new ExcelPackage(f);

                ExcelWorksheet ws = package.Workbook.Worksheets.Add("Data");
                ws.Cells["A1"].LoadFromDataTable(table, true);

                package.SaveAs(f);

                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }
            /*
            try
            {
                DataTable table = ConvertJsonToDataTable(json);
                Workbook workbook = new Workbook();
                Worksheet sheet = workbook.Worksheets[0];
                sheet.InsertDataTable(table, true, 1, 1);
                workbook.SaveToFile(path, ExcelVersion.Version2013);
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return false;
            }*/
        }
    }
}
