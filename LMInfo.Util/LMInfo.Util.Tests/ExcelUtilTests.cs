using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Diagnostics;
using System.IO;

namespace LMInfo.Util.Tests
{
    [TestClass]
    public class ExcelUtilTests
    {
        [TestMethod]
        public void TestWriteToExcel()
        {
            string json = string.Empty;
            string file = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "c:\\temp\\test.json");

            using (StreamReader reader = File.OpenText(file))
            {
                json = reader.ReadToEnd();
                reader.Close();
            }

            if (ExcelUtil.WriteToExcel(json, "c:\\temp\\test.xlsx"))
            {
                Debug.WriteLine("Success");
            }
            else
            {
                Debug.WriteLine("Failure");
            }
        }
    }
}
